-- Estructura del árbol genérico
data Arbol a = Vacio | Nodo a [Arbol a] deriving Show

tree :: Arbol Integer
tree = Nodo 100 [a11, a12, a13]
    where
        a11 = Nodo 22 [hoja 15, hoja 77]
        a12 = hoja 77
        a13 = Nodo 52 [hoja 33]

hoja :: tree -> Arbol tree
hoja x = Nodo x []

-- Función para determinar la raíz del árbol
raiz :: Arbol tree -> tree
raiz (Nodo x _) = x

-- Función para determinar el tamano (número de elementos en el árbol)
tamanio :: Arbol tree -> Integer
tamanio Vacio = 0
tamanio (Nodo _ xs) = (+1) . sum . map tamanio $ xs

-- Función para sumar los elementos del árbol
sumArbol :: Arbol Integer -> Integer
sumArbol (Nodo x xs) = sumar x (map sumArbol xs)
    where sumar n ns = n + sum ns

-- Ocurrencias del arbol
class Ocurre t where
    ocurre :: Eq a => a -> t a -> Integer

instance Ocurre Arbol where
    ocurre x Vacio = 0
    ocurre x (Nodo y ys)
        | x == y = 1 + sum(map (ocurre x) ys)
        | otherwise = sum(map (ocurre x) ys)

-- Valor máximo
class TieneMaximo t where
    maximo :: Ord a => t a -> a

instance TieneMaximo Arbol where
    maximo Vacio = error "Arbol Vacio"
    maximo (Nodo x []) = x
    maximo (Nodo x xs)
        | x >= maximum (map maximo xs) = x
        | otherwise = maximum (map maximo xs)